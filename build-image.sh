#!/bin/bash

# Nome da imagem Docker
DOCKER_IMAGE="estudo-api:1.0"

# Nome do contêiner
CONTAINER_NAME="estudo-api"

# Verifique se o Docker está instalado
if ! command -v docker &> /dev/null
then
    echo "Docker não está instalado. Por favor, instale o Docker antes de continuar."
    exit 1
fi

# Limpar contêineres e imagens antigas
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker rmi $DOCKER_IMAGE

# Construir a imagem Docker
docker build -t $DOCKER_IMAGE .

echo "Imagem Docker construída com sucesso: $DOCKER_IMAGE"
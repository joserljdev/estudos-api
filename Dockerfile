# Usando a imagem base do OpenJDK para Java 11
FROM openjdk:11-jre-slim

# Definando o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copiando o arquivo JAR da aplicação para o contêiner
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

# Expondo a porta que a aplicação vai escutar
EXPOSE 8080

# Comando para iniciar a aplicação quando o contêiner for executado
CMD ["java", "-jar", "app.jar"]
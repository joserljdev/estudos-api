package br.com.joserljdev.estudo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.joserljdev.estudo.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
}

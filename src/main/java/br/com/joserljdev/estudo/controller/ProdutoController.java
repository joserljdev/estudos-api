package br.com.joserljdev.estudo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.joserljdev.estudo.model.Produto;
import br.com.joserljdev.estudo.repository.ProdutoRepository;

@RestController
@RequestMapping("/api/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;

	@GetMapping
	public List<Produto> listarProdutos() {
		return produtoRepository.findAll();
	}

	@GetMapping("/health")
	public String verificarDisponibilidadeServico() {
		return "Serviço disponível!";
	}

	@PostMapping
	public Produto criarProduto(@RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}

	@GetMapping("/{id}")
	public Produto buscarProdutoPorId(@PathVariable Long id) {
		return produtoRepository.findById(id).orElse(null);
	}

	@PutMapping("/{id}")
	public Produto atualizarProduto(@PathVariable Long id, @RequestBody Produto produtoAtualizado) {
		Produto produto = produtoRepository.findById(id).orElse(null);
		if (produto != null) {
			produto.setNome(produtoAtualizado.getNome());
			produto.setPreco(produtoAtualizado.getPreco());
			return produtoRepository.save(produto);
		}
		return null;
	}

	@DeleteMapping("/{id}")
	public void deletarProduto(@PathVariable Long id) {
		produtoRepository.deleteById(id);
	}
}

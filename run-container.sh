#!/bin/bash

# Nome do contêiner (sem a tag)
CONTAINER_NAME="estudo-api"

# Nome da imagem Docker
DOCKER_IMAGE="estudo-api:1.0"

# Porta do host mapeada para a porta do contêiner
HOST_PORT=8080

# Verifica se o Docker está instalado
if ! command -v docker &> /dev/null
then
    echo "Docker não está instalado. Por favor, instale o Docker antes de continuar."
    exit 1
fi

# Verifica se o contêiner já está em execução e o para
docker stop $CONTAINER_NAME

# Remove o contêiner antigo
docker rm $CONTAINER_NAME

# Execute o contêiner Docker
docker run -p $HOST_PORT:8080 --name $CONTAINER_NAME $DOCKER_IMAGE